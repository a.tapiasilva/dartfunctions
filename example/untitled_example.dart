import 'package:untitled/untitled.dart';

void main() {
  saludos("Adrian");

  String mensaje = saludar("Mateo");
  print(mensaje);

  wena("Abelardo","rodriguez",24);

  buena();

  print(factorial(4).toString());


}

void saludos(nombre){ //De forma express puede ser así: void saludos(nombre) => print("Bienvenido $nombre");
  print("Bienvenido $nombre");
}

String saludar(nombre){
  String mensaje = "Bienvenido nombre 2 $nombre";
  return mensaje;
}

//Parametros opcionales se encierran entre brackets.

void wena(String nombre,[String apellido, num edad]){
  if(apellido != null && edad != null){
    print("Bienvenido $nombre despues del if");
  }else{
    print("Bienvenido $nombre despues del else");
  }

}

void buena({String nombre="Anonimo",String apellido=""}){
  var salud = new StringBuffer("Otra vez aquí");
  if(nombre != null){
    salud.write(" $nombre");
  }
  if(apellido != null){
    salud.write("$apellido");
  }
  
  print(salud.toString());

}

//Función recursiva debe llamarse así misma para resolverse

num factorial(num n){
  if(n == 1){
    return 1;
  }else {
    return n * factorial(n-1);
  }
}
